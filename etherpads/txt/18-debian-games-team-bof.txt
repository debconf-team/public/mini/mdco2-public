== Debian Games Team BoF ==

The purpose of the Debian Games Team is to coordinate, share common problems and solutions and to maintain games collaboratively

Games team contains everything from actual games to game emulators, gaming utilities. and game engines.

Packages are maintained on salsa, debian's git server:
    https://salsa.debian.org/games-team

Packages maintained by games team:
    https://qa.debian.org/developer.php?login=pkg-games-devel@lists.alioth.debian.org

Website / Wiki:
    https://wiki.debian.org/Games/Team

IRC Channel:
    #debian-games on the oftc network

Maintstats
    https://blends.debian.org/games/maintstats/

== Discussion / Questions? ==

* category for utilities in .desktop files?
* backports
* sponsorship requests

QUESTION:
Can you talk a little about packaging games with non-free artwork but free source code and how that relates to the use of contrib and to game downloaders.
    https://osgameclones.com/

Do I need to be a DM/DD to join the team? How does one go about joining?
Is any (free) game welcome to be team-maintained by debian-games-team?
I'm upstream, other than licence check and dependency check, what can I do to make packaging easier?
    https://wiki.debian.org/UpstreamGuide

What would you consider more (potentially) complicated to package, a game or a library?
    libraries are more difficult
    For example Mindustry #959466 ... "and then you need a whole new programming language packaging"
          Not to derail the discussion, but I've been through that. I was studying a certain software for packaging and ended up going down a rabbit hole of requirements :(

Are there objective criteria on splitting binary packages (-data, -extra, etc)?
- architecture-independent files should better go into an arch:all package (e.g. -data), and game binaries into an arch:any package
- only necessary to split when there's a significant amount of data. It's usually done to reduce install size or archive size.
- gaming packages often have a separate headless -server package

Comment from lightning talk: try installing all the games in debian and then finding the one you want in the menu. Please can the Games Team ensure e.g. correct classification in the desktop files. (debtags taxonomy was also mentioned in IRC).

=== RFS ===

tools:
    or settings

https://mentors.debian.net/package/gbsplay/
https://mentors.debian.net/package/goverlay/
https://mentors.debian.net/package/lutris/ 
https://mentors.debian.net/package/mangohud/
https://mentors.debian.net/package/vkbasalt/

games:

https://mentors.debian.net/package/c-evo/
https://mentors.debian.net/package/endless-sky/
https://mentors.debian.net/package/ironseed/
https://mentors.debian.net/package/pentobi/
https://mentors.debian.net/package/pinball-table-hurd/ (done, thanks yadd)
https://mentors.debian.net/package/speed-dreams/ (abandoned by uploader)
https://mentors.debian.net/package/starfighter/
https://mentors.debian.net/package/supertuxkart/ (backport)
https://mentors.debian.net/package/surgescript/
https://mentors.debian.net/package/zig-game/
