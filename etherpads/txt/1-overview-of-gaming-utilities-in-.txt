Notes from me:
	* Another cool project I haven't showcased: https://github.com/Plagman/gamescope
	* Links to packages that need sponsors: https://lists.debian.org/debian-devel-games/2020/11/msg00023.html
	* GOverlay 0.4.2 now uses Qt5 (on Mentors)
	* vkBasalt shipped headers have been removed upstream (no release yet)



Questions:

Q. Do the runners support using the packaged versions for e.g. wine or does it always require downloading a build from lutris?<emorrp1>
> I have actually no idea (never tested), but I think they always use the build from Lutris
> they do! You can choose whether you want to use the lutris wine (for example) or one already installed<highvoltage>

Q. Followup: can it also use Proton, if Steam (with Proton) is already installed?
 - highvoltage says: looking at the options in lutris it seems like they ship their own protonified runner
> Yes ot can use Proton as it would be a native Steam title.

Q. What is the difference between installing games from platforms that already have a GNU/Linux client (like Steam)?
> Actually there is not a big difference. Most Steam installers do nothing but adding a shortcut. The only advantage is that you can easily adjust settings like environmnet variables.
> If it's not on Steam, most installers just download the tarball. I haven't found an installer which let's you use for example apt (e. g. for Supertuxkart).

Q. Can Lutris be used fully offline, or does it need to download things (runners, runtimes, etc.) prior to a game installation?
> Afaik it works, but you won't be able to download runtimes ofc, so native or system wine only.

Q. What is ReplaySourcery?
An instant replay solution like Radeon ReLive, so you press a button and the last 30s will be put in a video.
