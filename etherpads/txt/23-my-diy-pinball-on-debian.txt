URL: https://pad.online.debconf.org/p/mdco2-23-my-diy-pinball-on-debian


== Questions ? ==

Q: What's the model of that big screen you're using? ~emorrp1
A: A digital signage screen, it's like a TV but supposed to be used outside
nowdays you see many diplaying ads on bus stops etc, I made an other use :-)
The one I saved from thrash is not in perfect condition for watching movies
but works enough for the pinball, it's plasma one it's super bright but heavy

Q: Is there any news you'd like to share since your talk was recorded ?
https://tracker.debian.org/pkg/pinball-table-hurd
[2020-11-21]                    Accepted pinball-table-hurd 0.0.20201119-1 (source all amd64) into unstable, unstable                (Debian FTP Masters)                        (signed by: Xavier Guimard)

Q: Is it possible have ramp in emilia pinball? --diego71
Yes using pinedit


== IRC Log ==

<bittin[m]> also Emilia Pinball will be 20 years this xmas :D 
<rzr> so i should release an update for 2020 xmass 
<emorrp1> (also convenient timing for bullseye upload)
<valhalla> that screen is small. mobian!
<rzr> :P
<rzr> it could a good candidate
<rzr> but I have no pine phone
<rzr> input controls could be missing but it's doable 
<bittin[m]> haven't played this in years maybe i should try it again
<rzr> play the gnu table
<rzr> it's fun
<valhalla> yeah, it will probably need some additional way to be controlled from the touchscreen?
<valhalla> currently installing it to see what happens
<rzr> not much
<rzr> just split screen in 2
<rzr> touch on bottom left side for left flipper 
<rzr> etc
<rzr> can you name this table ?
<bittin[m]> Pinball fantasies something
<rzr> partyland obviously 
<rzr> yea
<rzr> on amiga
<Fuddl_> hm, to increase visability of the new pinabll tables, the pinball package should suggest or recommend additional tables instead of having the packages of add-on tables depend on pinball
<bittin[m]> developed by DICE
<emorrp1> Fuddl_: spoilers :p
<rzr> Fuddl_, yes I'll do this once I adopt it
<bittin[m]> or Digital Illusions but now DICE
<Fuddl_> is it in the DGT salsa? I felt about to change it :)
<rzr> this DI table was made by community for FP
<pollo> cat!
<emorrp1> Fuddl_: https://salsa.debian.org/games-team/pinball
<rzr> I merged those patches upstream
<emorrp1> (needs vcs-git adding too
<rzr> yes that's on my plans
<rzr> uploaders would be help
<emorrp1> accelerometer nudges is a cool idea
<rzr> btw there is no questions yet
<rzr> at
<rzr> https://pad.online.debconf.org/p/mdco2-23-my-diy-pinball-on-debian
<rzr> I can copy them 
<rzr> if you want to ask them here
<emorrp1> QUESTION: what's the model of that big screen you're using?
<diego71> QUESTION: is it possible have ramp in emilia pinball?
<Fuddl_> I just tried out gnu and hurd tables. great work! :)
<emorrp1> QUESTION: is there any news you'd like to share since your talk was recorded ? 😉
<Tarnyko> rzr: "Autofools" ^^
<henqvist> greetings from the other developer :)
<rikardGn> hello coming to watch a few lectures and perhaps play some games in the brake
<emorrp1> henqvist: welcome
<axhn> Tarnyko: Is it time again for ISBN 3936546487 ?
<emorrp1> Die GNU Autotools: Leitfaden für die Softwaredistribution
<nattie> WAIT!  There was one more question ;)
<jathan> nattie: Haha, Thats right :)
<jathan> The last one.
<nattie> we're all doing each other's jobs here ;)
<Tarnyko> axhn: wut ???
<emorrp1> collab-talk-meister
<rzr> henqvist, hi
<highvoltage> thanks to you too rzr!
<rzr> so great to have you
<henqvist> great talk rzr
<axhn> Tarnyko: "die" is (here) just "the" (plural) in German
<emorrp1> 👏 and good luck on applying to be a DM
<rzr> so do you like to revive this ?
<rzr> I am a DM already maybe I could level up too
<rzr> henqvist, we did not talk for years 
<emorrp1> awesome, that would be 3 of us
<rzr> but we did not forget us :)
<jathan> rzr: Great talk and so interesting how you build the Pinball Cab!
<rzr> henqvist, is Emilia's author 
<henqvist> :D. oh lord, that was the first game I ever made, I can believe it's still compiling
<rzr> all the glory should be given back to henqvist :-)
<henqvist> :D. oh lord, that was the first game I ever made, I can't believe it's still compiling
<rzr> well it didn't evolve much
<rzr> I was a bad maintainer :)
<emorrp1> and so the Matrix users reveal themselves :p
<henqvist> rzr, big credits to you for keeping it alive
<rzr> I was too busy to do proper maintenance
<rzr> now I had more time to do stuff that i had my TODO stack
<Tarnyko> henqvist: I have Emilia pinball in my archives, thanks to this fellow ^^
<rzr> hi Tarnyko 
<henqvist> last time it tried to compile pinedit was over 10y ago, don't even know if one can find qt4 now
<Tarnyko> hi rzr
<rzr> henqvist, well I rebuilt pinedit last week
<bittin[m]> yeah thanks henqvist used to play it back in the days 
<rzr> it just worked on debian-10
<Tarnyko> most LTS distros still provide qt4, X11 only of course
<henqvist> I actually modellel everything with it
<rzr> what about bulleye ?
<rzr> henqvist, you did ?
<rzr> how many hours wasted :)
<henqvist> light years behind a real 3d editor of course, was quite a pain :D
<rzr> henqvist, did you know about other community tables ?
<rzr> henqvist, something that would be awesome is to convert or reimplement FuturePinball table
<rzr> there great other pinball projects on windows
<henqvist> actual no, I know some people contacted me and asked about pinedit but I think it was too difficult to use
<rzr> VPX is also gaining traction
<henqvist> adding vertices by hand etc
<rzr> yea that's why I attempted to load 3ds models 
<henqvist> but it did have undo, which I was very proud of back in those days
<rzr> but collision detection was not yet supported
<henqvist> no collision detection in editor
<rzr> glTF format can be promising
<rzr> I was talking about my ugly 3DS parser
<rzr> I made for the ugly "house" table
<henqvist> glTF would be nice, of course renderer would need to be rewritten
<rzr> i hacked some alternative renderers visitors
<henqvist> :+1:
<rzr> but no noticable gain yet
<rzr> well Intergration took lot of time
<rzr> It's only a hobby project to test new tech
<striker> I don't see his screen
<rzr> but if there is traction from community the project can catch up time
<rzr> I wont do this alone
<rzr> henqvist, should we update sf's homepage for 20years anniversary ?
<rzr> for legacy :)
<bittin[m]> heh
<jathan> rzr: Thanks for pointing that.
<henqvist> is it 20 years already?
<rzr> looks like yes
<jathan> henqvist: Great talk and so interesting how you built your Pinball Cab!
<rzr> we'll do it again for 40th ;)


== Notes ==

Feedback also welcome on Fediverse:
https://purl.org/rzr/pinball 

Slides published at:
https://purl.org/rzr/presentations


Video published at:
https://purl.org/rzr/videos
https://archive.org/download/pinball-debconf-2020-rzr/pinball-debconf-2020-rzr.webm


News: pinball-table-gnu & pinball-table-hurd packages:
https://qa.debian.org/developer.php?login=rzr@users.sf.net
